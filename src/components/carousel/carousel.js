$(document).ready(function() {
	$('.carousel_one-direction .carousel__track').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		infinite: true,
		arrows: true,
		appendDots: $('.carousel'),
		prevArrow: '<a class="arrow carousel__arrow carousel__arrow_prev" href="javascript:void(0)"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="306px" height="306px" viewBox="0 0 306 306" xml:space="preserve"><g><polygon points="94.35,0 58.65,35.7 175.95,153 58.65,270.3 94.35,306 247.35,153 "/></g></svg></a>',
		nextArrow: '<a class="arrow carousel__arrow carousel__arrow_next" href="javascript:void(0)"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="306px" height="306px" viewBox="0 0 306 306" xml:space="preserve"><g><polygon points="94.35,0 58.65,35.7 175.95,153 58.65,270.3 94.35,306 247.35,153 "/></g></svg></a>',
		responsive: [
			{
				breakpoint: 960,
				settings: {
					arrows: false,
					slidesToShow: 3
				}
			},
			{
				breakpoint: 450,
				settings: {
					slidesToShow: 2,
					arrows: false
				}
			}
		]
	})
	$('.carousel_classic .carousel__track').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		infinite: true,
		arrows: true,
		dots: false,
		prevArrow: '<a class="arrow carousel__arrow carousel__arrow_prev" href="javascript:void(0)"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="306px" height="306px" viewBox="0 0 306 306" xml:space="preserve"><g><polygon points="94.35,0 58.65,35.7 175.95,153 58.65,270.3 94.35,306 247.35,153 "/></g></svg></a>',
		nextArrow: '<a class="arrow carousel__arrow carousel__arrow_next" href="javascript:void(0)"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="306px" height="306px" viewBox="0 0 306 306" xml:space="preserve"><g><polygon points="94.35,0 58.65,35.7 175.95,153 58.65,270.3 94.35,306 247.35,153 "/></g></svg></a>',
		responsive: [
			{
				breakpoint: 960,
				settings: {
					dots: false,
					arrows: true,
					slidesToShow: 2
				}
			},
			{
				breakpoint: 720,
				settings: {
					dots: false,
					arrows: true,
					slidesToShow: 1
				}
			},
			{
				breakpoint: 540,
				settings: {
					dots: false,
					arrows: false,
					slidesToShow: 1
				}
			}
		]
	})
	$('.carousel_short-classic .carousel__track').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		infinite: true,
		arrows: true,
		dots: false,
		prevArrow: '<a class="arrow carousel__arrow carousel__arrow_prev" href="javascript:void(0)"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="306px" height="306px" viewBox="0 0 306 306" xml:space="preserve"><g><polygon points="94.35,0 58.65,35.7 175.95,153 58.65,270.3 94.35,306 247.35,153 "/></g></svg></a>',
		nextArrow: '<a class="arrow carousel__arrow carousel__arrow_next" href="javascript:void(0)"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="306px" height="306px" viewBox="0 0 306 306" xml:space="preserve"><g><polygon points="94.35,0 58.65,35.7 175.95,153 58.65,270.3 94.35,306 247.35,153 "/></g></svg></a>',
		responsive: [
			{
				breakpoint: 960,
				settings: {
					dots: false,
					arrows: true,
					slidesToShow: 2
				}
			},
			{
				breakpoint: 720,
				settings: {
					dots: false,
					arrows: true,
					slidesToShow: 1
				}
			},
			{
				breakpoint: 540,
				settings: {
					dots: false,
					arrows: false,
					slidesToShow: 1
				}
			}
		]
	})

	const singleLineSettings = {
		slidesToShow: 1,
		slidesToScroll: 1,
		infinite: true,
		arrows: true,
		dots: false,
		prevArrow: '<a class="arrow carousel__arrow carousel__arrow_prev" href="javascript:void(0)"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="306px" height="306px" viewBox="0 0 306 306" xml:space="preserve"><g><polygon points="94.35,0 58.65,35.7 175.95,153 58.65,270.3 94.35,306 247.35,153 "/></g></svg></a>',
		nextArrow: '<a class="arrow carousel__arrow carousel__arrow_next" href="javascript:void(0)"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="306px" height="306px" viewBox="0 0 306 306" xml:space="preserve"><g><polygon points="94.35,0 58.65,35.7 175.95,153 58.65,270.3 94.35,306 247.35,153 "/></g></svg></a>',
		responsive: [
			{
				breakpoint: 540,
				settings: 'unslick'
			}
		]
	}

	const singleLineCarousel = $('.carousel_single-line .carousel__track').slick(singleLineSettings);

	$(window).on('resize', function() {
		if (window.outerWidth > 540 &&  !singleLineCarousel.hasClass('slick-initialized')) {
			$('.carousel_single-line .carousel__track').slick(singleLineSettings);
		}
	});
})