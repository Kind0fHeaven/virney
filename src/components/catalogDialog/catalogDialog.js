$(document).ready(function() {
	
	$('.catalog-button__inner').click(function() {

		$(this).toggleClass('catalog-button__inner_active').parent().find('.catalog-dialog').toggleClass('catalog-dialog_opened')

	})

	$('.catalog-dialog__item_submenu').click(function() {
		
		const menu = $(this).attr('data-menu');
		const level = parseInt($(this).closest('.catalog-dialog__menu').attr('data-menu-level'));

		if (level === 1) {
			$('.catalog-dialog').addClass('catalog-dialog_second-level').removeClass('catalog-dialog_third-level');
			$('.catalog-dialog__menu[data-menu-level="3"]').removeClass('catalog-dialog__menu_active');
			$('.catalog-dialog__menu[data-menu-level="2"] > .catalog-dialog__item_active').removeClass('catalog-dialog__item_active')
		}

		if (level === 2) {
			$('.catalog-dialog').addClass('catalog-dialog_third-level');
		}

		$(this).toggleClass('catalog-dialog__item_active');
		$(`.catalog-dialog__menu[data-menu=${menu}]`).toggleClass('catalog-dialog__menu_active')

		if (!$('.catalog-dialog__menu[data-menu-level="2"]').hasClass('catalog-dialog__menu_active')) {
			$('.catalog-dialog').removeClass('catalog-dialog_second-level')
		}
		if (!$('.catalog-dialog__menu[data-menu-level="3"]').hasClass('catalog-dialog__menu_active')) {
			$('.catalog-dialog').removeClass('catalog-dialog_third-level');
		}

	})

	$('.catalog-dialog__cross').click(function() {
		$('.catalog-dialog').removeClass('catalog-dialog_opened');
		$('.catalog-button__inner').removeClass('catalog-button__inner_active');
	})

	$('.catalog-dialog__title').click(function() {
		if (window.outerWidth < 540) {
			$('.catalog-dialog').removeClass('catalog-dialog_opened');
			$('.catalog-button__inner').removeClass('catalog-button__inner_active');
		}
	})

	$('.catalog-dialog__back').click(function() {
		if ($('.catalog-dialog__menu[data-menu-level="3"]').hasClass('catalog-dialog__menu_active')) {
			$('.catalog-dialog__menu[data-menu-level="3"]').removeClass('catalog-dialog__menu_active')
			$('.catalog-dialog__menu[data-menu-level="2"] .catalog-dialog__item_active').removeClass('catalog-dialog__item_active')
			$('.catalog-dialog').removeClass('catalog-dialog_third-level');
		} else {
			if ($('.catalog-dialog__menu[data-menu-level="2"]').hasClass('catalog-dialog__menu_active')) {
				$('.catalog-dialog__menu[data-menu-level="2"]').removeClass('catalog-dialog__menu_active')
				$('.catalog-dialog__menu[data-menu-level="1"] .catalog-dialog__item_active').removeClass('catalog-dialog__item_active')
				$('.catalog-dialog').removeClass('catalog-dialog_second-level');
			}
		}
	})


	$(document).mouseup(function (e){
		if (!$('.catalog-button').is(e.target) && $('.catalog-button').has(e.target).length === 0) {
			$('.catalog-dialog').removeClass('catalog-dialog_opened');
			$('.catalog-button__inner').removeClass('catalog-button__inner_active');
		}
	});
})