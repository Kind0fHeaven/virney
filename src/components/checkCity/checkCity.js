jQuery(document).ready(function() {
	$('.check-city__answer-no').click(function() {
		$('.check-city').addClass('check-city_wrong')
	})

	$('.overlay, .check-city__answer-yes, .check-city__button').click(closeCheckCity)

	function closeCheckCity() {
		$('.check-city, .overlay').fadeOut()
	}
})