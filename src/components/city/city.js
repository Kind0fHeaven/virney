$(document).ready(function() {

	$('.city__open').click(function() {

		if (window.outerWidth < 540) {
			$('body, html').addClass('modal_opened');
			$('.check-city, .overlay').fadeOut();
			$('.city_menu').toggleClass('city_opened');
			$('.menu').addClass('menu_opened')
			return 0;
		}

		if (window.outerWidth < 960) {
			$('body, html').addClass('modal_opened');
			$('.check-city, .overlay').fadeOut();
			$('.navbar .city__open').toggleClass('city__button_opened')
			$('.city_main').toggleClass('city_opened');
		} else {
			$(this).toggleClass('city__button_opened');
			$(this).parent().find('.city').toggleClass('city_opened')
			if ($(this).parent().find('.city').hasClass('city_first')) {
				new SimpleBar($(this).parent().find('.city__list')[0], { autoHide: false })
			}
		}
		

	})

	$(window).on('resize',function() {
		if (window.outerWidth < 960) {
			$('.city__list[data-simplebar="init"]').each(function() {
				var content = $(this).find('.simplebar-content').html()
				$(this).html(content);
			})
		}
	})

	$('.city__cross, .city__title > .icon').click(function() {


		$(this).closest('.city').removeClass('city_opened')
		$(this).closest('.city').parent().find('.city__open').removeClass('city__button_opened')
		$('body, html').removeClass('modal_opened');

	})

	$(document).mouseup(function (e){
		if (!$('.city').is(e.target) && $('.city').has(e.target).length === 0 && !$('.city__open').is(e.target) && $('.city__open').has(e.target).length === 0) {
			$('.city').removeClass('city_opened');
			$('.city').parent().find('.city__open').removeClass('city__button_opened');
			$('body, html').removeClass('modal_opened');
		}
	});

})