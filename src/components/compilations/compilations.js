$(document).ready(function() {
	
	function compilationResize() {

		if (window.innerWidth < 540) {
			$('.compilations__item').removeClass('compilations__item_hidden');
			$('.compilations__dropdown').removeAttr('style').find('.compilations__item').remove();

			$('.compilations__item').each(function() {
				
				const elem = $(this).clone();

				if ($(this).index() > 2) {
					$('.compilations__dropdown').append(elem);
					$(this).addClass('compilations__item_hidden')
				}

			})

			$('.compilations__more').addClass('compilations__more_shown');

			return false;
		}

		const availableWidth = window.innerWidth - 110;
		
		let sum = 0;

		$('.compilations__dropdown').removeAttr('style').find('.compilations__item').remove();
		$('.compilations__more').removeClass('compilations__more_shown');

		$('.compilations__item').removeClass('compilations__item_hidden').each(function() {
			
			sum += $(this).outerWidth() + 20;

			if (sum < availableWidth) return true;

			const elem = $(this).clone();

			$('.compilations__dropdown').append(elem);

			$(this).addClass('compilations__item_hidden');

		})

		if ($('.compilations__item_hidden').length) {
			$('.compilations__more').addClass('compilations__more_shown');
			$('.compilations__dropdown').css('left', $('.compilations__list').outerWidth());

			if ($('.compilations__dropdown').outerWidth() > window.innerWidth - $('.compilations__list').outerWidth() - 30) {
				$('.compilations__dropdown').removeAttr('style')
			}
		} else {
			$('.compilations__more').removeClass('compilations__more_opened');
			$('.compilations__dropdown').removeClass('compilations__dropdown_opened');
		}

	}

	$('.compilations__more').click(function() {
		$(this).toggleClass('compilations__more_opened').parent().children('.compilations__dropdown').toggleClass('compilations__dropdown_opened');


		if ($(this).hasClass('compilations__more_opened') && window.innerWidth < 540) {
			$('body, html').addClass('modal_opened');
			$('.show-filter').addClass('show-filter_passed')
		}
	})

	$('.compilations__cross').click(function() {
		$('.compilations__more').removeClass('compilations__more_opened').parent().children('.compilations__dropdown').removeClass('compilations__dropdown_opened');
		$('body, html').removeClass('modal_opened');
		$('.show-filter').removeClass('show-filter_passed')
	})

	compilationResize();

	$(window).resize(compilationResize);

})