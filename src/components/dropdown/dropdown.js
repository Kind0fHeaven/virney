$(document).ready(function() {
	$(document).mouseup(function (e){
		if (!$('.dropdown').is(e.target)
		    && $('.dropdown').has(e.target).length === 0) {
			$('.dropdown').removeClass('dropdown_opened');
		}
	});
})

$('.dropdown__active').click(function() {

	const block = $(this).parent();

	if (block.hasClass('dropdown_opened')) {
		block.removeClass('dropdown_opened')
	} else {
		block.addClass('dropdown_opened')
	}

})