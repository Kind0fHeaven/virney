$(document).ready(function() {
	
	$('.filter__title').click(function() {
		
		const block = $(this).closest('.filter__block')

		block.toggleClass('filter__block_opened')
	
	})


	$('.alphabet__dropdown').click(function() {
		
		const block = $(this).closest('.alphabet')

		block.toggleClass('alphabet_opened')
	
	})

	$('.filter__show-more').click(function() {
		
		const block = $(this).closest('.filter__content').children('.filter__list')

		block.toggleClass('filter__list_opened')
		$(this).toggleClass('filter__show-more_opened')
	
	})

	$('.filter__single-color').click(function() {

		$(this).toggleClass('filter__single-color_checked')
	
	})

	const filterSliderSettings = {
		min: 10000,
		max: 2000000,
		from: 100000,
		to: 1100000,
		step: 10000
	}

	const filterSlider = $('.filter__slider-track').slider({
		range: true,
		min: filterSliderSettings.min,
		max: filterSliderSettings.max,
		values: [filterSliderSettings.from, filterSliderSettings.to],
		step: filterSliderSettings.step,
		slide: function(event,ui) {

			let from = ui.values[0]+'';
			let to = ui.values[1]+'';
			
			from = from.replace(/[^\d]/g, '').replace(/\B(?=(?:\d{3})+(?!\d))/g, ' ')
			to = to.replace(/[^\d]/g, '').replace(/\B(?=(?:\d{3})+(?!\d))/g, ' ')
			$('#priceFrom').val(from);
			$('#priceTo').val(to);
		}
	})
	
	$('#priceFrom').val((filterSliderSettings.from+'').replace(/[^\d]/g, '').replace(/\B(?=(?:\d{3})+(?!\d))/g, ' '));
	$('#priceTo').val((filterSliderSettings.to+'').replace(/[^\d]/g, '').replace(/\B(?=(?:\d{3})+(?!\d))/g, ' '));


	$('.filter__input').on('input', function() {
		let value = parseInt(this.value.replace(/ /g,''), 10);
		let number = $(this).attr('id') === 'priceFrom' ? 0 : 1;

		if (number === 0 && value > parseInt($('#priceTo').val().replace(/ /g,''), 10)) {
			value = parseInt($('#priceTo').val().replace(/ /g,''), 10);
		} else if (value < parseInt($('#priceFrom').val().replace(/ /g, ''), 10)) {
			value = parseInt($('#priceFrom').val().replace(/ /g, ''), 10);
		}

		if (!value) value = 0;

		$(this).val((value+'').replace(/[^\d]/g, '').replace(/\B(?=(?:\d{3})+(?!\d))/g, ' '));

		
		filterSlider.slider("values", number, value);
	})


	$('.show-filter').click(function() {
		$('.filter').addClass('filter_shown');
		$('body, html').addClass('modal_opened');
	})

	$('.filter__cross').click(function() {
		$('.filter').removeClass('filter_shown');
		$('body, html').removeClass('modal_opened');
	})
})