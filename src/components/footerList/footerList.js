jQuery(document).ready(function() {
	
	jQuery('.footer-list__title').click(function() {
		
		if (window.outerWidth >= 540 || jQuery(this).parent().hasClass('footer-list_info')) return 0
		
		jQuery(this).parent().toggleClass('footer-list_opened')
	})

})