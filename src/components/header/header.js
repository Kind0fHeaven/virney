const menuOpener = $('.js__menu-open');
const menuCloser = $('.js__menu-close');


menuOpener.click(() => menuOpen());
menuCloser.click(() => menuClose());


function menuOpen() {
	
	$('body, html').addClass('modal_opened');
	$('.menu').addClass('menu_opened');

	document.ontouchmove = function (e) {e.preventDefault()}
}

function menuClose() {

	$('body, html').removeClass('modal_opened');
	$('.menu').removeClass('menu_opened');
	
	document.ontouchmove = function (e) {return true}

}


window.addEventListener('resize', function() {
	if (window.outerWidth >= 540) {
		menuClose()
	}
});