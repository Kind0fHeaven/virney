$(document).ready(function() {

	$('.preview__open').click(function() {
		
		if (window.outerWidth < 720) return false;

		$(this).toggleClass('preview__open_active');
		
		$(this).parent().find('.items-preview').toggleClass('items-preview_opened');

	})

	$('.items-preview__cross').click(function() {

		$(this).closest('.items-preview').parent().find('.preview__open').removeClass('preview__open_active');
		
		$(this).parent().removeClass('items-preview_opened')

	})

	$(window).resize(function() {
		
		if (window.outerWidth < 720) {
			$('.preview__open').removeClass('preview__open_active')
			$('.items-preview').removeClass('items-preview_opened')
		}

	})

	$(document).mouseup(function (e){
		if (!$('.items-preview').is(e.target)
		    && $('.items-preview').has(e.target).length === 0 && !$('.preview__open').is(e.target) && $('.preview__open').has(e.target).length === 0) {
			$('.preview__open').removeClass('preview__open_active')
			$('.items-preview').removeClass('items-preview_opened')
		}
	});

})