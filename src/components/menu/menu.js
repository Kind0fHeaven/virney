if (window.outerWidth < 540) {
	setVerticalHeight();
}

window.addEventListener('resize', () => setVerticalHeight());

function setVerticalHeight() {
	let vh = window.innerHeight * 0.01;
	document.documentElement.style.setProperty('--vh', `${vh}px`);
}