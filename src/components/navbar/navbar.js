$(document).ready(function() {

	$('.navbar__account--open').click(function() {
		
		if (window.outerWidth < 540) return false;

		$('.navbar__account').toggleClass('navbar__account_opened');

	})


	$('.navbar__account--cross').click(function() {
		$('.navbar__account').removeClass('navbar__account_opened');
	})

	$(document).mouseup(function (e){
		if (!$('.navbar__account').is(e.target) && $('.navbar__account').has(e.target).length === 0 && !$('.navbar__account--open').is(e.target) && $('.navbar__account--open').has(e.target).length === 0) {
			$('.navbar__account').removeClass('navbar__account_opened');
		}
	});

}) 