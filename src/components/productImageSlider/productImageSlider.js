$(document).ready(function() {
    jQuery('.product-image__slider').slick({
        dots: false,
        arrows: false,
        infinite: true,
        asNavFor: '.product-image__nav',
    })
    jQuery('.product-image__nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.product-image__slider',
        dots: false,
        arrows: true,
        focusOnSelect: true,
        infinite: true,
        centerMode: true,
        centerPadding: 0,
        prevArrow: '<a class="arrow product-image__arrow product-image__arrow_prev" href="javascript:void(0)"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="306px" height="306px" viewBox="0 0 306 306" xml:space="preserve"><g><polygon points="94.35,0 58.65,35.7 175.95,153 58.65,270.3 94.35,306 247.35,153 "/></g></svg></a>',
        nextArrow: '<a class="arrow product-image__arrow product-image__arrow_next" href="javascript:void(0)"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="306px" height="306px" viewBox="0 0 306 306" xml:space="preserve"><g><polygon points="94.35,0 58.65,35.7 175.95,153 58.65,270.3 94.35,306 247.35,153 "/></g></svg></a>',
        responsive: [
            {
                breakpoint: 960,
                settings: {
                    slidesToShow: 3,
                    centerMode: false,
                    arrows: false
                }
            },
            {
                breakpoint: 540,
                settings: {
                    slidesToShow: 2,
                    centerMode: false,
                    arrows: false
                }
            },
        ]
    });
})