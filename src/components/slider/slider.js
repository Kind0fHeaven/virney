$(document).ready(function() {
	
	$('.slider__track').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		infinite: true,
		dots: true,
		arrows: true,
		dotsClass: 'slider__dots',
		appendDots: $('.slider'),
		prevArrow: '<a class="slider__arrow arrow slider__arrow_prev" href="javascript:void(0)"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="306px" height="306px" viewBox="0 0 306 306" xml:space="preserve"><g><polygon points="94.35,0 58.65,35.7 175.95,153 58.65,270.3 94.35,306 247.35,153 "/></g></svg></a>',
		nextArrow: '<a class="slider__arrow arrow slider__arrow_next" href="javascript:void(0)"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="306px" height="306px" viewBox="0 0 306 306" xml:space="preserve"><g><polygon points="94.35,0 58.65,35.7 175.95,153 58.65,270.3 94.35,306 247.35,153 "/></g></svg></a>',
		responsive: [
			{
				breakpoint: 720,
				settings: {
					arrows: false
				}
			}
		]
	})

})