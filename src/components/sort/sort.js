$(document).ready(function() {
	$(document).mouseup(function (e){
		if (!$('.sort').is(e.target)
		    && $('.sort').has(e.target).length === 0) {
			$('.sort').removeClass('sort_opened');
		}
	});
})

$('.sort__active').click(function() {

	const block = $(this).parent();

	if (block.hasClass('sort_opened')) {
		block.removeClass('sort_opened')
	} else {
		block.addClass('sort_opened')
	}

})