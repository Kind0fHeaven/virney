$('.tab').click(function() {

	if ($(this).hasClass('tab_active')) return 0;

	$(this).parent().find('.tab_active').removeClass('tab_active');
	$(this).addClass('tab_active')

	const tab = $(this).attr('data-tab');

	$('.tab__body.tab__body_active').removeClass('tab__body_active');
	$(`.tab__body[data-tab=${tab}]`).addClass('tab__body_active')

})


$('.tab__title').click(function() {
	$(this).parent().toggleClass('tab__body_active');

	const tab = $(this).parent().attr('data-tab');
	$(`.tab[data-tab=${tab}]`).toggleClass('tab_active')
})