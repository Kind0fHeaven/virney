$(document).ready(function() {
	$('.order-page__total--more').click(function() {
		$(this).toggleClass('order-page__total--more_opened')
		$(this).parent().find('.order-page__total--drop').toggleClass('order-page__total--drop_opened')
	})

	$(document).mouseup(function (e){
		if (!$('.order-page__total--drop').is(e.target)
		    && $('.order-page__total--drop').has(e.target).length === 0 && !$('.order-page__total--more').is(e.target)
		    && $('.order-page__total--more').has(e.target).length === 0) {
			$('.order-page__total--drop').removeClass('order-page__total--drop_opened');
			$('.order-page__total--more').removeClass('order-page__total--more_opened');
		}
	});
})